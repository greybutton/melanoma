var express = require('express');
var router = express.Router();

var weight = {
  skin: 1,
  hair: 1,
  freckle: 2,
  ultraviolet: 2,
  period: 1,
  family: 3,
};
var sumOfWeight = 10;

/* GET home page. */
router.post('/', function(req, res, next) {
  const userAnswer = req.body;
  const sumOfAnswer = Object.entries(userAnswer).reduce((acc, [key, value]) => {
    if (value === 'yes') {
      return acc + weight[key];
    }
    return acc;
  }, 0);
  const result = sumOfAnswer / sumOfWeight;
  res.json({ result: result });
});

module.exports = router;
