$(document).ready(function() {
  $('#exampleModal').modal({
    show: true
  });

  $('.js-send').on('click', send);

  function send() {
    var skin = $('[name="skin"]:checked').val();
    var hair = $('[name="hair"]:checked').val();
    var freckle = $('[name="freckle"]:checked').val();
    var ultraviolet = $('[name="ultraviolet"]:checked').val();
    var period = $('[name="period"]:checked').val();
    var family = $('[name="family"]:checked').val();

    var data = {
      skin: skin,
      hair: hair,
      freckle: freckle,
      ultraviolet: ultraviolet,
      period: period,
      family: family,
    };

    $.ajax({
      type: 'POST',
      url: '/result',
      data: data,
      dataType: 'JSON',
      success: function(response) {
        var result = 'По введенным данным шанс развития рака кожи из данного новообразования ' + response.result * 100 + '%';
        $('.js-result').append(
          '<div class="modal fade" id="resultModal" tabindex="-1" role="dialog" aria-labelledby="resultModalLabel" aria-hidden="true" data-show="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="resultModalLabel">Результат</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">' + result + '</div><div class="modal-footer"><button type="button" class="btn btn-secondary js-result-close" data-dismiss="modal">Закрыть</button></div></div></div></div>'
        );
        $('#resultModal').modal({
          show: true
        });
        $('.js-result-close').on('click', function() {
          setTimeout(function() {
            $('#resultModal').remove();
          }, 300);
        });
      }
    });
  };
});

function bs_input_file() {
	$(".input-file").before(
		function() {
			if ( ! $(this).prev().hasClass('input-ghost') ) {
				var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0; position: absolute;'>");
				element.attr("name",$(this).attr("name"));
				element.change(function(){
					element.next(element).find('input').val((element.val()).split('\\').pop());
				});
				$(this).find("button.btn-choose").click(function(){
					element.click();
				});
				$(this).find("button.btn-reset").click(function(){
					element.val(null);
					$(this).parents(".input-file").find('input').val('');
				});
				$(this).find('input').css("cursor","pointer");
				$(this).find('input').mousedown(function() {
					$(this).parents('.input-file').prev().click();
					return false;
				});
				return element;
			}
		}
	);
}
$(function() {
	bs_input_file();
});